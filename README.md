# Adidas Code Challenge

Within each project there are more detailed specifications.

Subscriber service: https://gitlab.com/slavcho87/subscriber

Email service: https://gitlab.com/slavcho87/email

# Pipeline

The CI/CD process of this project has been configured in two stages. The first is the compilation of the project and the second is the building of a docker image. This docker image is stored in the gitlab repository and this project itself is configured to make use of these images and deploy them to your local environment. You don't have to do anything but have docker installed and run the command indicated in the Build and Run section.

The subscriber and email projects have their repository and are configured in such a way that every time they upload changes in the main branch (main in this case because we don't have pre and pro environments) the gitlab pipeline will be executed to generate the images and leave them in the repository.

For the generation of the gitlab images we have used a concept called runner that is specific to gitlab. In this case we have created our own runner that has been executed in my local environment. The section Gitlab Runner explains how to configure your own runner.

# Environment installation

The only thing you have to do is to clone the project and execute the command in the following section.

    git clone https://gitlab.com/slavcho87/adidas

# Build and Run
To build and run the docker image run the following command:

    docker-compose up -d

This command will install the subscriber service, email and apache kafka and you will be ready to use them. In the subscriber repository you can find a section about how to use the api.

# Apacke kafka
The apache kafka configuration will create the sendEmail topic automatically.

An important thing to mention is the configuration of the docker ports. The image build creates one port that can be used externally (localhost:9092) and one that can be used internally (kafka:9093). The subscriber and email are now intended to run on the internal docker network and are therefore configured for this.

All dockers who want to see the apache kafka broker should join the adidas_default network. This is created by default with the image build.

# Gitlab Runner - Windows instalation
Go to the gitlab page and download gitlab-runner: https://docs.gitlab.com/runner/install/windows.html. Once you have gitlab-runner we have to create two runners: one for subscriver service and one for email service. This is done using the command:

    gitlab-runner.exe register --url https://gitlab.com/ --registration-token $TOKEN

where $TOKEN is a token found in the repository > settings > CI/CD > Runners. Then we get a console wizard that will ask us for some serious data. We only have to indicate that we want to use docker and linux for the runner.

Once the runner is created we see that a file called config.toml has been created and we look for the property privileged = true of each runner and we make sure that it is true because otherwise we will not have privileged subicients to generate the docker images.

